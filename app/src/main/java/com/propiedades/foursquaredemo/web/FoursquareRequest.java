package com.propiedades.foursquaredemo.web;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by condesa on 19/09/17.
 */

public class FoursquareRequest {

    private static final String ENDPOINT = "https://api.foursquare.com/";
    private static final String LAST_UPDATE_DATE = "20151126";
    private static final String API_RESPONSE_MODE = "foursquare";
    private static final String SEARCH_REQUEST = "search";

    public static final String FOURSQUARE_CLIENT_ID = "PTMFLIQTT5LFZKASLEFG4FJ4QY1EV5YMVXSGEN5QHNICZL4A";
    public static final String FOURSQUARE_CLIENT_SECRET = "ALN1KIMVOVGWHGBBXRXSQMPRGOIOGC24NQ4QGJHG2LFDSSEU";

    public static final String FOURSQUARE_FOOD_CATEGORY = "4d4b7105d754a06374d81259";

    public static FoursquareService getService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(FoursquareService.class);
    }

    public interface FoursquareService {
        @Headers("Accept-Language: es")
        @GET("v2/venues/{requestType}")
        Call<JsonObject> getDetails(@Path("requestType") String requestType, @Query("ll") String latlng,
                                    @Query("radius") int radius, @Query("limit") int limit,
                                    @Query("categoryId") String categories, @Query("client_id") String clientId,
                                    @Query("client_secret") String clientSecret, @Query("v") String lastUpdate,
                                    @Query("m") String responseMode);
    }
}
